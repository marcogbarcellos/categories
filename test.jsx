// Define a collection to hold our tasks
Tasks = new Mongo.Collection("tasks");

if (Meteor.isClient) {
  

  Meteor.subscribe("tasks");

  Meteor.startup(function () {
    // Use Meteor.startup to render the component after the page is ready
    ReactDOM.render(<VimCar />, document.getElementById("render-target"));
  });
}

if (Meteor.isServer) {
  // Only publish tasks that are public or belong to the current user
  Meteor.publish("tasks", function () {
    return Tasks.find();
  });
}

Meteor.methods({
  addTask(text) {
    
    Tasks.insert({
      text: text,
      createdAt: new Date(),
    });
  },

  removeTask(taskId) {
    const task = Tasks.findOne(taskId);
    
    Tasks.remove(taskId);
  }

});
