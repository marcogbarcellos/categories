// App component - represents the whole app
VimCar = React.createClass({

  // This mixin makes the getMeteorData method work
  mixins: [ReactMeteorData],

  getInitialState() {
    return { filterText: '' }
  },
  
  stateUpdate(value){
    this.setState({ filterText: value });
  },

  // Loads items from the Tasks collection and puts them on this.data.tasks
  getMeteorData() {
    let query = {};

    return {
      tasks: Tasks.find(query, {sort: {createdAt: -1}}).fetch()
    };
  },

  handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    var text = React.findDOMNode(this.refs.textInput).value.trim();

    Meteor.call("addTask", text);

    // Clear form
    React.findDOMNode(this.refs.textInput).value = "";
  },
  filterTrigger() {
    // sending the new filter value to the parent component
    this.stateUpdate(this.refs.filterInput.value);
    
  },
  render() {
    return (
      <div className="container">
        <header>
          <div className="row">
          <h2 className="col-md-12">VimCar Test</h2>

          <h4 className="col-md-12">First of all, thanks for this opportunity.</h4> 
          <h4 className="col-md-12">This app is built with ReactJS+meteor(Nodejs and mongodb are included too).</h4> 
          </div>
          <div className="row">
            <form className="col-md-6" onSubmit={this.handleSubmit} >
              <input
                type="text"
                ref="textInput"
                placeholder="&#xF067; Type to add new tasks" />
            </form>
            <form className="col-md-6">
              <input
                className="pull-right" 
                type='text' 
                ref='filterInput'
                placeholder='&#xF002; Type to filter' 
                value={this.state.filterText}
                onChange={this.filterTrigger} />
            </form>
          </div>
        </header>

        <Categories
        tasks={this.data.tasks}
        dataApp={this.props.data} 
        filter={this.state.filterText} />
      </div>
    );
  }
});
