// Categories component - represents the whole set of categories
Categories = React.createClass({
  propTypes: {
    tasks: React.PropTypes.object.isRequired,
  },
  render() {
    var input = this.props.filter;
    var categories = this.props.tasks.map(function(category, key) {
      if (!category.text.indexOf(input)) { 
      return <Task
        key={category._id}
        task={category}
        filter={input} />;
      }else{
        return; 
      }
    });
    return (
      <ul>
        {categories}
      </ul>
    )
    
    
  }
});
