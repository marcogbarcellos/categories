// Task component - represents a single category item
Task = React.createClass({
  propTypes: {
    task: React.PropTypes.object.isRequired,
  },
  getInitialState: function () {
        return {hover: false};
  },
  
  mouseOver: function () {
      this.setState({hover: true});
  },
  
  mouseOut: function () {
      this.setState({hover: false});
  },
  deleteThisTask() {
    Meteor.call("removeTask", this.props.task._id);
  },

  render() {
    var input = this.props.filter;
    var renderText = this.props.task.text; 
    if (!this.state.hover && this.props.task.text && this.props.task.text.length>10) {
        renderText = this.props.task.text.substring(0,10)+"...";
    }
    if (!this.props.task.text.indexOf(input)) { 
        return (<li className="category"
                    onMouseOver={this.mouseOver}
                    onMouseOut={this.mouseOut}>
                  <button className="delete" onClick={this.deleteThisTask}>
                    &times;
                  </button>

                  <span className="text">
                    {renderText}
                  </span>
                </li>)
      }else{
        return <li></li>; 
      }
    
  }
});
